<?php

namespace Metasyntactical\PHPUnit\Constraint\Xml;

use DOMDocument;
use SimpleXMLElement;

/**
 * Asserts that a given xml complies with a given xml schema.
 *
 * @param string $schema
 * @param SimpleXMLElement|DOMDocument|string $xml
 * @param string $message
 */
function assertXmlIsValidatedBySchema($schema, $xml, $message = '')
{
    Functions::assertXmlIsValidatedBySchema($schema, $xml, $message);
}
