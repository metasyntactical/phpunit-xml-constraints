<?php

namespace Metasyntactical\PHPUnit\Constraint\Xml;

use DOMDocument;
use InvalidArgumentException;
use LibXMLError;
use PHPUnit\Framework\Constraint\Constraint as AbstractConstraint;
use SimpleXMLElement;

final class XsdValidationConstraint extends AbstractConstraint
{
    /**
     * @var string
     */
    private $schema;

    /**
     * @var LibXMLError[]
     */
    private $errors = [];

    /**
     * {@inheritdoc}
     */
    public function __construct(string $schemaPathNameOrContents)
    {
        parent::__construct();

        $this->schema = $schemaPathNameOrContents;

        if (!$this->schemaIsXml() && !file_exists($schemaPathNameOrContents)) {
            throw new InvalidArgumentException("Schema {$this->schema} does not exist.");
        }
    }

    /**
     * {@inheritdoc}
     */
    public function toString(): string
    {
        return "validates against XSD schema".($this->schemaIsXml() ? '' : " '".basename($this->schema)."'");
    }

    /**
     * {@inheritdoc}
     */
    protected function matches($other)
    {
        $previousUseInternalErrors = libxml_use_internal_errors(true);

        if ($other instanceof SimpleXMLElement) {
            $dom = new DOMDocument();
            $dom->appendChild($dom->importNode(dom_import_simplexml($other), true));
        } elseif ($other instanceof DOMDocument) {
            $dom = $other;
        } elseif (strpos($other, '<') !== false) {
            $dom = new DOMDocument();
            $dom->loadXML($other);
        } else  {
            $dom = new DOMDocument();
            $dom->load($other);
        }

        $returnValue = $this->schemaIsXml()
            ? $dom->schemaValidateSource($this->schema)
            : $dom->schemaValidate($this->schema);

        if (!$returnValue) {
            $this->errors = libxml_get_errors();
        }

        libxml_use_internal_errors($previousUseInternalErrors);

        return $returnValue;
    }

    protected function additionalFailureDescription($other)
    {
        $description = [];

        foreach ($this->errors as $error) {
            $description[] = " - ".trim($error->message);
        }

        return implode("\n", $description);
    }

    protected function failureDescription($other)
    {
        if ($other instanceof SimpleXMLElement) {
            $xml = $other->asXML();
        } elseif ($other instanceof DOMDocument) {
            $xml = $other->saveXML();
        } else {
            $xml = $other;
        }

        return "{$xml} {$this->toString()}";
    }

    private function schemaIsXml(): bool
    {
        return strpos($this->schema, '<') !== false;
    }
}
