<?php

namespace Metasyntactical\PHPUnit\Constraint\Xml;

use DOMDocument;
use SimpleXMLElement;

trait XmlAssertionsTrait
{
    /**
     * Asserts that a given xml complies with a given xml schema.
     *
     * @param string $schema
     * @param SimpleXMLElement|DOMDocument|string $xml
     * @param string $message
     */
    public static function assertXmlIsValidatedBySchema($schema, $xml, $message = '')
    {
        $constraint = new XsdValidationConstraint($schema);

        static::assertThat($xml, $constraint, $message);
    }
}
