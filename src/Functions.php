<?php

namespace Metasyntactical\PHPUnit\Constraint\Xml;

use PHPUnit\Framework\Assert;

final class Functions extends Assert
{
    use XmlAssertionsTrait;
}
