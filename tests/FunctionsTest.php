<?php

namespace Metasyntactical\PHPUnit\Constraint\Xml;

use Metasyntactical\Data\XmlNamespaces\W3C\NamespaceLocator;
use PHPUnit\Framework\ExpectationFailedException;
use PHPUnit\Framework\TestCase;

class FunctionsTest extends TestCase
{
    /**
     * @dataProvider provideValidXmlSchema()
     */
    public function testXsdValidationAssertion($schema, $xml)
    {
        Functions::assertXmlIsValidatedBySchema($schema, $xml);
    }

    /**
     * @dataProvider provideValidXmlSchema()
     */
    public function testXsdValidationConstraint($schema, $xml)
    {
        $constraint = new XsdValidationConstraint($schema);

        self::assertStringStartsWith("validates against XSD schema", $constraint->toString());
    }

    public function provideValidXmlSchema()
    {
        $domDocument = new \DOMDocument('1.0');
        $domDocument->loadXML(<<<EOXML_
<?xml version="1.0" encoding="UTF-8"?>
<example:example xmlns:example="http://example.com/schema/example-schema">Example</example:example>
EOXML_
        );

        return [
            'xsd' => [
                NamespaceLocator::locate('http://www.w3.org/2001/XMLSchema'),
                <<<EOXML_
<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns="http://example.com/schema/example-schema"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            elementFormDefault="qualified"
            targetNamespace="http://example.com/schema/example-schema">
    <xsd:element name="example" type="xsd:string"/>
</xsd:schema>
EOXML_
            ],
            'example' => [
                <<<EOXML_
<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns="http://example.com/schema/example-schema"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            elementFormDefault="qualified"
            targetNamespace="http://example.com/schema/example-schema">
    <xsd:element name="example" type="xsd:string"/>
</xsd:schema>
EOXML_
                ,
                <<<EOXML_
<?xml version="1.0" encoding="UTF-8"?>
<example:example xmlns:example="http://example.com/schema/example-schema">Example</example:example>
EOXML_
            ],
            'example-domdocument' => [
                <<<EOXML_
<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns="http://example.com/schema/example-schema"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            elementFormDefault="qualified"
            targetNamespace="http://example.com/schema/example-schema">
    <xsd:element name="example" type="xsd:string"/>
</xsd:schema>
EOXML_
                ,
                $domDocument
            ],
            'example-simplexml' => [
                <<<EOXML_
<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns="http://example.com/schema/example-schema"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            elementFormDefault="qualified"
            targetNamespace="http://example.com/schema/example-schema">
    <xsd:element name="example" type="xsd:string"/>
</xsd:schema>
EOXML_
                ,
                simplexml_import_dom($domDocument)
            ],
        ];
    }

    public function testXsdValidationFails()
    {
        $schema =<<<_EOXML_
<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns="http://example.com/schema/example-schema"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            elementFormDefault="qualified"
            targetNamespace="http://example.com/schema/example-schema">
    <xsd:element name="example" type="xsd:string"/>
</xsd:schema>
_EOXML_;
        $xml =<<<EOXML_
<foo/>
EOXML_;

        $constraint = new XsdValidationConstraint($schema);

        $result = $constraint->evaluate($xml, '', true);

        self::assertFalse($result);

        $this->expectException(ExpectationFailedException::class);
        $this->expectExceptionMessage("Failed asserting that <foo/> validates against XSD schema.\n - Element 'foo': No matching global declaration available for the validation root.");
        $constraint->evaluate($xml);
    }
}
